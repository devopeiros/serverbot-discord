# Server-Bot

[Ver no Docker Hub](https://hub.docker.com/r/allenrkeen/server-bot)
### Bot do Discord para monitorar e controlar remotamente um servidor baseado em Docker. Usando o socket do Docker.

A configuração é bastante simples.
1. Crie um novo aplicativo no *[portal de desenvolvedores do Discord](https://discord.com/developers/applications)*.
2. Vá para a seção de bots e clique em *Adicionar Bot*.
3. Regenere o Token e guarde-o em um local seguro (Isso será referido como "DISCORD_TOKEN" em .env e variáveis de ambiente do Docker).
4. Obtenha o "ID do Aplicativo" na aba Informações Gerais do seu aplicativo (Isso será referido como "DISCORD_CLIENT_ID" em .env e variáveis de ambiente do Docker).
5. *Opcional:* Se você tiver o modo desenvolvedor ativado no Discord, obtenha o ID do seu servidor clicando com o botão direito no nome do servidor e clicando em "Copiar ID" (Isso será referido como "DISCORD_GUILD_ID" em .env e variáveis de ambiente do Docker).
   - Se você pular isso, ainda funcionará, mas os comandos serão publicados globalmente em vez de no seu servidor, podendo levar até uma hora para ficarem disponíveis no seu servidor.
   - Usar o ID do Servidor será mais seguro, tornando os comandos disponíveis apenas no servidor especificado.
6. Execute o aplicativo da maneira que preferir.
   - Execute o container Docker com o arquivo [docker-compose.yml](docker-compose.yml) fornecido ou use o comando docker run abaixo.

      ```bash
      docker run -v /var/run/docker.sock:/var/run/docker.sock --name server-bot \
      -e DISCORD_TOKEN=seu_token_aqui \
      -e DISCORD_CLIENT_ID=seu_id_de_cliente_aqui \
      -e DISCORD_GUILD_ID=seu_id_de_servidor_aqui \
      allenrkeen/server-bot:latest
      ```

   - Clone o repositório, navegue para o diretório server-bot e execute "npm install" para instalar as dependências, depois "npm run start" para iniciar o servidor.
7. O programa irá criar um link de convite com as permissões corretas e colocá-lo nos logs. Clique no link e confirme o servidor para adicionar o bot.

Comandos atuais:
  - /allcontainers
    - fornece o nome e o status do contêiner para todos os contêineres
  - /restartcontainer
    - fornece uma lista de preenchimento automático de contêineres em execução para selecionar, ou apenas digite o nome do contêiner e reinicie-o
  - /stopcontainer
    - fornece uma lista de preenchimento automático de contêineres em execução para selecionar, ou apenas digite o nome do contêiner e pare-o
  - /startcontainer
    - fornece uma lista de preenchimento automático de contêineres parados para selecionar, ou apenas digite o nome do contêiner e inicie-o
  - /ping
    - Responde com "Pong!" quando o bot está ouvindo
  - /server
    - Responde com o Nome do Servidor e a contagem de membros, bom para testes.